package api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.response.ValidatableResponse;
import com.jayway.restassured.specification.RequestSpecification;
import vo.Bear;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static utils.PropertyLoader.getProperty;

public class BearsServices {

    RequestSpecification rs;

    BearsServices() {
        this.rs = given().log().all();
        RestAssured.baseURI = getProperty("baseUrl");
    }

    public static BearsServices bearsServices() {
        return new BearsServices();
    }

    public ValidatableResponse readByGet(String url, int statusCode) {
        return rs.get(url).then().statusCode(statusCode)
                .contentType(ContentType.HTML)
                .defaultParser(Parser.JSON);
    }

    public Bear[] getAllBears() {
        return readByGet(baseURI + "/bear", 200)
                .extract().as(Bear[].class);
    }

    public Bear getBearById(int id) {
        return readByGet(baseURI + "/bear/" + id, 200)
                .extract().as(Bear.class);
    }

    public String getBearByIdWithError(int id) {
        return readByGet(baseURI + "/bear/" + id, 200)
                .extract().as(String.class);
    }

    public ValidatableResponse addByPost(String body, int statusCode) {
        return rs.contentType(ContentType.JSON).body(body)
                .accept(ContentType.JSON).
                post(baseURI + "/bear")
                .then().statusCode(statusCode)
                .contentType(ContentType.HTML);
    }

    public Integer addBear(String body) {
        return addByPost(body, 200)
                .defaultParser(Parser.JSON)
                .extract()
                .as(Integer.class);
    }

    public ValidatableResponse addBearWithError(String body) {
        return addByPost(body, 500);
    }

    public ValidatableResponse updateByPut(String body, int id, int statusCode) {
        return rs.body(body)
                .put(baseURI + "/bear/" + id)
                .then().statusCode(statusCode)
                .contentType(ContentType.HTML);
    }

    public String updateBear(String body, int id) {
        return updateByPut(body, id, 200)
                .defaultParser(Parser.JSON)
                .extract().as(String.class);
    }

    public ValidatableResponse updateBearWithError(String body, int id) {
        return updateByPut(body, id, 500);
    }

    public ValidatableResponse deleteByDelete(String url, int statusCode) {
        return rs.when().delete(url)
                .then().statusCode(statusCode);
    }

    public ValidatableResponse deleteAllBears() {
        return deleteByDelete(baseURI + "/bear", 200);
    }

    public ValidatableResponse deleteBearById(int id) {
        return deleteByDelete(baseURI + "/bear/" + id, 200);
    }

}
