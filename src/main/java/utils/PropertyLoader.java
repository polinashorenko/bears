package utils;

import java.util.Properties;

public class PropertyLoader {
    private static final String propertyFile = "/application.properties";

    public static String getProperty(String propertyName) {
        Properties props = new Properties();
        try {
            props.load(PropertyLoader.class.getResourceAsStream(propertyFile));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String value = props.getProperty(propertyName);

        return value;
    }

}
