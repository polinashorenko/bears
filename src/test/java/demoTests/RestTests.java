package demoTests;

import org.javatuples.Triplet;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import vo.Bear;

import java.util.ArrayList;
import java.util.List;

import static api.BearsServices.bearsServices;
import static java.util.Arrays.asList;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RestTests {

    private String bearJson = "{\"bear_type\":\"%s\",\"bear_name\":\"%s\",\"bear_age\":%s}";

    @BeforeAll
    public static void clean() {
        bearsServices().deleteAllBears();
    }

    @Order(1)
    @ParameterizedTest
    @MethodSource("validBears")
    public void testAddValidBear(Triplet bear) {
        String name = bear.getValue1().toString();
        String type = bear.getValue0().toString();
        Double age = Double.valueOf(bear.getValue2().toString());
        String body = String.format(bearJson, type, name, age);

        int id = bearsServices().addBear(body);

        Bear addedBear = bearsServices().getBearById(id);
        assertAll("check added bear",
                () -> assertTrue(addedBear.bear_id > 0),
                () -> assertThat(addedBear.bear_name).isEqualTo(name),
                () -> assertThat(addedBear.bear_type).isEqualTo(type),
                () -> assertThat(addedBear.bear_age).isEqualTo(age)
        );
    }

    @Order(2)
    @Test
    public void testGetAllBears() {
        assertThat(bearsServices().getAllBears()).isNotEmpty();
    }

    @Order(3)
    @ParameterizedTest
    @MethodSource("names")
    public void testUpdateBearName(String name) {
        String body = "{\"bear_name\":\"%s\"}";
        int id = bearsServices().getAllBears()[0].bear_id;
        bearsServices().updateBear(String.format(body, name), id);
        assertThat(bearsServices().getBearById(id).bear_name).isEqualTo(name);
    }

    @Order(4)
    @ParameterizedTest
    @MethodSource("types")
    public void testUpdateBearType(String type) {
        String body = "{\"bear_type\":\"%s\"}";
        int id = bearsServices().getAllBears()[0].bear_id;
        String oldType = bearsServices().getAllBears()[0].bear_type;

        bearsServices().updateBearWithError(String.format(body, type), id);
        assertThat(bearsServices().getBearById(id).bear_type).isEqualTo(oldType);
    }

    @Order(5)
    @Test
    public void testUpdateBearAge() {
        String body = "{\"bear_age\":%s}";
        int id = bearsServices().getAllBears()[0].bear_id;
        double age = bearsServices().getAllBears()[0].bear_age;

        bearsServices().updateBearWithError(String.format(body, 12.8), id);
        assertThat(bearsServices().getBearById(id).bear_age).isEqualTo(age);
    }

    @Order(6)
    @Test
    public void testUpdateBear() {
        String type = "BLACK";
        String name = "Mikhail";
        double age = 12.8;
        Bear bear = bearsServices().getAllBears()[0];
        bearsServices().updateBear(String.format(bearJson, type, name, age), bear.bear_id);

        Bear updatedBear = bearsServices().getBearById(bear.bear_id);
        assertAll("check updated bear",
                () -> assertThat(updatedBear.bear_name).isEqualTo(name),
                () -> assertThat(updatedBear.bear_type).isEqualTo(bear.bear_type),
                () -> assertThat(updatedBear.bear_age).isEqualTo(bear.bear_age)
        );
    }

    @Order(7)
    @Test
    public void testAddInvalidBear() {
        String type = "black";
        String name = "Mikhail";
        double age = 12.8;

        int size = bearsServices().getAllBears().length;
        bearsServices().addBearWithError(String.format(bearJson, type, name, age));
        assertEquals(size, bearsServices().getAllBears().length, "Invalid bear was added");
    }

    @Order(8)
    @Test
    public void testDeleteBearById() {
        int id = bearsServices().getAllBears()[0].bear_id;
        bearsServices().deleteBearById(id);
        assertThat(bearsServices().getBearByIdWithError(id)).isEqualTo("EMPTY");
    }

    @Order(9)
    @Test
    public void testDeleteAllBears() {
        assertThat(bearsServices().getAllBears()).isNotEmpty();
        bearsServices().deleteAllBears();
        assertThat(bearsServices().getAllBears()).isEmpty();
    }

    private static List<Triplet<String, String, Double>> validBears() {
        List<Triplet<String, String, Double>> data = new ArrayList<>();
        data.add(new Triplet("POLAR", "Mishka", 17.5));
        data.add(new Triplet("BROWN", "potap", 16.0));
        data.add(new Triplet("BLACK", "TOPTOP", 10));
        data.add(new Triplet("GUMMY", "Taptap", 10.9));
        return data;
    }

    private static List<String> names() {
        return asList("Mimimishka", "mimimishka", "MIMIMISHKA");
    }

    private static List<String> types() {
        return asList("POLAR", "BROWN", "BLACK", "GUMMY");
    }

}
